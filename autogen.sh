#! /bin/sh -e
test -n "$srcdir" || srcdir=`dirname "$0"`
test -n "$srcdir" || srcdir=.
(
    cd "$srcdir" &&
    gnome-doc-common --copy &&
    gnome-doc-prepare --automake --copy --force &&
    mm-common-prepare --copy --force "$srcdir" &&
    autopoint --force &&
    AUTOPOINT='intltoolize --automake --copy' autoreconf --force --install --verbose "$srcdir"
) || exit
test -n "$NOCONFIGURE" || "$srcdir/configure" "$@"
