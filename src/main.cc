/*
 *  Glom PostgreSQL Setup - main.cc
 *
 *  Copyright (C) 2011 Openismus GmbH
 *
 *  Author: Jon Nordby <jonn@openismus.com>
 *  Author: Chis Kühl <chrisk@openismus.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtkmm.h>

#include "config-dialog.h"

int main(int argc, char *argv[])
{
  Gtk::Main kit(argc, argv);
  ConfigDialog window("Glom PostgreSQL Setup");

  Gtk::Main::run(window);
}
