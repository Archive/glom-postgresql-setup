/*
 *  Glom PostgreSQL Setup - config-dialog.h
 *
 *  Copyright (C) 2011 Openismus GmbH
 *
 *  Author: Jon Nordby <jonn@openismus.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLOM_POSTGRESQL_SETUP_UI_H
#define GLOM_POSTGRESQL_SETUP_UI_H

#include <gtkmm.h>

#include "configuration.h"

/** Dialog for setting up configuration.
 * This is used as the main window. */
class ConfigDialog : public Gtk::Dialog
{
public:
  explicit ConfigDialog(const Glib::ustring& title);

private:
  /* Child widgets */
  Gtk::Label m_description_label;
  Gtk::Label m_error_label;

  Gtk::Table m_input_table;

  /* Table elements */
  Gtk::Label m_username_label;
  Gtk::Entry m_username_entry;

  Gtk::Label m_password_label;
  Gtk::Entry m_password_entry;

  Gtk::Label m_password_confirm_label;
  Gtk::Entry m_password_confirm_entry;

  Gtk::Label m_config_path_label;
  Gtk::Entry m_config_path_entry;
  Gtk::Button m_config_path_button;

  /* Internal members*/
  Configuration config;

  /* Internal functions */
  void initialize_widgets();
  void response_accept();
  virtual void on_response(int response);
  void set_path_from_file_chooser();
};

#endif //GLOM_POSTGRESQL_SETUP_UI_H
