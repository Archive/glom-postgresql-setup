/*
 *  Glom PostgreSQL Setup - configuration.h
 *
 *  Copyright (C) 2011 Openismus GmbH
 *
 *  Author: Chis Kühl <chrisk@openismus.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLOM_POSTGRESQL_SETUP_ERROR_CODES_H
#define GLOM_POSTGRESQL_SETUP_ERROR_CODES_H

#include <glibmm/fileutils.h>

namespace SuccessCodes
{

enum Changes
{
  CHANGES_APPLIED,
  CHANGES_NOT_NEEDED
};

};

namespace ErrorCodes
{

enum Misc
{
  MISC_NOT_ROOT = SuccessCodes::CHANGES_NOT_NEEDED + 1,
  MISC_BAD_ARGUMENTS,
  MISC_PROGRAM_EXITED_ABNORMALLY,
  MISC_AUTHENTICATION_FAILED = 127 // This is what pkexec returns if the
                                   // user could not get root access.
};

enum DbConnection
{
  DB_CONNECTION_NULL = MISC_PROGRAM_EXITED_ABNORMALLY + 1,
  DB_CONNECTION_BAD
};

enum UserCreation
{
  USER_ALREADY_EXISTS = DB_CONNECTION_BAD + 1,
  USER_DISALLOWED_CHAR,
  USER_NAME_EMPTY,
  USER_PASSWORDS_DONT_MATCH,
  USER_PASSWORD_EMPTY
};

enum ConfigFile
{
  CONFIG_PATH_EMPTY = USER_PASSWORD_EMPTY + 1,
  CONFIG_FILE_NOT_FOUND,
  CONFIG_FILE_ERROR,
  CONFIG_IO_STATUS_ERROR,
  CONFIG_RENAME_FAILED
};

};

#endif // GLOM_POSTGRESQL_SETUP_ERROR-CODES_H
