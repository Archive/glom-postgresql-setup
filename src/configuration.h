/*
 *  Glom PostgreSQL Setup - configuration.h
 *
 *  Copyright (C) 2011 Openismus GmbH
 *
 *  Author: Jon Nordby <jonn@openismus.com>
 *  Author: Chis Kühl <chrisk@openismus.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLOM_POSTGRESQL_SETUP_CONFIGURATION_H
#define GLOM_POSTGRESQL_SETUP_CONFIGURATION_H

#include <gtkmm.h>

/** Holds the necessary state and does validation of input.
 */
class Configuration
{
public:
  Configuration();

  void set_username(const Glib::ustring& username);
  void set_password(const Glib::ustring& password);
  void set_config_path(const std::string& config_path);

  Glib::ustring get_default_username() const;
  Glib::ustring get_username() const;
  Glib::ustring get_password() const;
  std::string get_config_path() const;

  int apply() const;

private:
  Glib::ustring m_username;
  Glib::ustring m_password;
  std::string m_config_path;
};

#endif // GLOM_POSTGRESQL_SETUP_CONFIGURATION_H

