/*
 *  Glom PostgreSQL Setup - glom-postgresql-setup.cc
 *
 *  Copyright (C) 2011 Openismus GmbH
 *
 *  Author: Jon Nordby <jonn@openismus.com>
 *  Author: Chris Kühl <chrisk@openismus.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Actions that require privileged access. These are implemented seperately
 * from the main application because privilege escalation using PolicyKit
 * requires the privileged parts to be executed in a separate process.
 * Consecuently we also need to do IPC between this and the main application. */

#include <iostream> /* for debug/printing to std::cout */
#include <cstdio> /* for rename() */
#include <cstdlib> /* for getenv() */

#include <glibmm.h>

#include "error-codes.h"

#include "config.h"

// Using the C library as it's more likely to be installed.
#include "libpq-fe.h"

// Needed for getpwnam and struct passwd.
#include <sys/types.h>
#include <pwd.h>

// For error reporting to stderr.
#include <string.h>
#include <errno.h>

#include <glibmm/i18n.h>

// The value of port should be extracted from the config file
// and then used in the database connection string.
static int port = -1;

static void print_error_msg(const Glib::ustring& error_msg)
{
  std::cerr << error_msg << std::endl;
}

static void get_port_from_line(const Glib::ustring &line)
{
  // We extract the port by using a named backreference.
  Glib::RefPtr<Glib::Regex> regex =
    Glib::Regex::create("^\\s*port\\s*=\\s*(\\d{1,5}).*");
  Glib::MatchInfo match_info;
  if(regex->match(line, match_info))
  {
    // Get and convert first (and only) backreference.
    std::istringstream(match_info.fetch(1)) >> port;
  }
}

static int create_postgre_user(const Glib::ustring& username, const Glib::ustring& password)
{
  // Check that input contains only alphanumeric chars and underscore.
  Glib::RefPtr<Glib::Regex> regex = Glib::Regex::create("^\\w+$");
  if(!regex->match(username))
  {
    print_error_msg("ERROR: Disallowed characters in username.");
    return ErrorCodes::USER_DISALLOWED_CHAR;
  }

  if(!regex->match(password))
  {
    print_error_msg("ERROR: Disallowed characters in password.");
    return ErrorCodes::USER_DISALLOWED_CHAR;
  }

  // Get the uid of the postgres user.
  struct passwd *pwd;
  pwd = getpwnam("postgres");

  // Change effective uid of the calling process.
  setuid(pwd->pw_uid);

  /*
    We're using the following default value for the following.

    * host = localhost
    * dbname = name of user which is postgres.
    * user = name of os name of the user which now is postgres.
    * password = not needed because the postgres user is admin of the postgres
    db.

    The default connection timeout is indefinite, which is not what we want.
    2 sec is recommended in the libpq documentation and what we use.

    An attempt to extract the port from the config file was made in the
    set_listen_on_all_interfaces function. If this failed then we use the
    default, which was set at compile time (usually 5432).
  */
  const int MAX_CONN_STRING_LENGTH = 32;
  char conn_string[MAX_CONN_STRING_LENGTH];
  if(port > 0)
    sprintf(conn_string, "port=%d connect_timeout=2", port);
  else
    sprintf(conn_string, "connect_timeout=2");

  PGconn *psql_conn = PQconnectdb(conn_string);

  // Error checking.
  if(!psql_conn)
  {
    print_error_msg("ERROR: Connection struct = NULL");
    return ErrorCodes::DB_CONNECTION_NULL;
  }

  if(PQstatus(psql_conn) != CONNECTION_OK)
  {
    print_error_msg(PQerrorMessage(psql_conn));
    PQfinish(psql_conn);
    return ErrorCodes::DB_CONNECTION_BAD;
  }

  // Create command string.
  const Glib::ustring create_user_sql =
    Glib::ustring::compose("CREATE USER %1 WITH PASSWORD '%2'",
                     username, password);

  // Execute the command.
  PGresult *result = PQexec(psql_conn, create_user_sql.c_str());

  if(PQresultStatus(result) != PGRES_COMMAND_OK)
  {
    print_error_msg(PQresultErrorMessage(result));
    PQclear(result);
    PQfinish(psql_conn);
    // We assume that the command is correct.
    return ErrorCodes::USER_ALREADY_EXISTS;
  }

  // Clean up
  PQclear(result);
  PQfinish(psql_conn);

  return SuccessCodes::CHANGES_APPLIED;
}

/* Modify the postgresql configuration to listen to all interfaces
 * Typically requires the calling process to be privileged,
 * depending on the permissions on config_file_path.
 * Return true on success, false on failure. */
static int set_listen_on_all_interfaces(const std::string& config_file_path)
{
  /* Note that typically only overwriting the configuration file with
   * our new file requires privileged access. But in some cases,
   * reading from the configuration file might also require
   * privileged access. */

  //const char* listen_addresses = "listen_addresses";
  const char* new_listen_addresses_line = "listen_addresses = '*'\n";

  // Test that file exists.
  if(!Glib::file_test(config_file_path, Glib::FILE_TEST_EXISTS))
  {
    print_error_msg("ERROR: Could not find PostrgreSQL config file" + config_file_path);
    return ErrorCodes::CONFIG_FILE_NOT_FOUND;
  }

  /* Open files */
  /* Using temp file path based on config_file_path instead of
   * mkstemp because the latter will often be on a different filesystem,
   * and rename would not handle that. */
  const std::string write_path = config_file_path + "~";
  Glib::RefPtr<Glib::IOChannel> write_channel;
  Glib::RefPtr<Glib::IOChannel> read_channel;
  try
  {
    write_channel = Glib::IOChannel::create_from_file(write_path, "w");
    read_channel  = Glib::IOChannel::create_from_file(config_file_path, "r");
  }
  catch(const Glib::FileError& error)
  {
    print_error_msg(error.what());
    return ErrorCodes::CONFIG_FILE_ERROR;
  }

  // Below, we'll need to find the line where the listen_addresses is set.
  Glib::RefPtr<Glib::Regex> listen_address_pattern =
    Glib::Regex::create("^\\s*listen_addresses.*");
  Glib::RefPtr<Glib::Regex> desired_listen_address_pattern =
    Glib::Regex::create("^\\s*listen_addresses\\s*=\\s*'[*]'.*");

  // Keep track of wether a change was actually needed. This is needed to
  // inform the user whether a server restart is needed.
  bool config_file_modified = false;

  /* Build modified configuration file, writing it to write_channel */
  // Set this to true whether we set this or it was already set correctly.
  bool setting_done = false;
  Glib::ustring line;
  Glib::IOStatus read_status, write_status = Glib::IO_STATUS_NORMAL;
  while(true)
  {
    read_status = read_channel->read_line(line);
    if(read_status == Glib::IO_STATUS_ERROR ||
        read_status == Glib::IO_STATUS_EOF)
      break;

    if(read_status == Glib::IO_STATUS_AGAIN)
      continue;

    // Extract the port number if we haven't already.
    if(port < 0)
      get_port_from_line(line);

    // Check for the listen_address setting and make changes if it's
    // not what we want.
    if(listen_address_pattern->match(line))
    {
      if(!desired_listen_address_pattern->match(line))
      {
        write_status = write_channel->write(new_listen_addresses_line);
        config_file_modified = true;
      }
      else
      {
        write_status = write_channel->write(line);
      }

      // Regardless of whether we made the change or not we marks as done.
      setting_done = true;
    }
    else
    {
      write_status = write_channel->write(line);
    }

    if(write_status == Glib::IO_STATUS_ERROR)
      break;
  }

  if(!setting_done)
  {
    /* No existing setting found, just append the right one */
    write_status = write_channel->write(new_listen_addresses_line);
    config_file_modified = true;
  }

  read_channel->close();
  write_channel->close();

  if(write_status == Glib::IO_STATUS_ERROR ||
      read_status  == Glib::IO_STATUS_ERROR)
  {
    print_error_msg("ERROR: IO status error");
    return ErrorCodes::CONFIG_IO_STATUS_ERROR;
  }

  /* Move the new file over the old one */
  const char* from = write_path.c_str();
  const char* to = config_file_path.c_str();
  if(rename(from, to))
  {
    print_error_msg(strerror(errno));
    return ErrorCodes::CONFIG_RENAME_FAILED;
  }

  // Return value indicates whether the file was changed or not.
  return config_file_modified ? SuccessCodes::CHANGES_APPLIED :
    SuccessCodes::CHANGES_NOT_NEEDED;
}

int main(int argc, char *argv[])
{
  // Basic check that we are in fact root.
  if(getuid() != 0) // FIXME: Probably not portable nor potable. ;)
  {
    print_error_msg("ERROR: glom-postgres-helper must be run as root. Exiting");
    return ErrorCodes::MISC_NOT_ROOT;
  }

  Glib::ustring username;
  Glib::ustring password;
  Glib::ustring config_path;

  Glib::OptionGroup options("glom_setup", _("Glom PostgreSQL setup options"));

  Glib::OptionEntry username_option;
  username_option.set_long_name("username");
  username_option.set_description(_("Username of new PostgreSQL user."));
  options.add_entry(username_option, username);

  Glib::OptionEntry password_option;
  password_option.set_long_name("password");
  password_option.set_description(_("Password of new PostgreSQL user."));
  options.add_entry(password_option, password);

  Glib::OptionEntry config_path_option;
  config_path_option.set_long_name("config-path");
  config_path_option.set_description(_("PostgreSQL configuration file path."));
  options.add_entry(config_path_option, config_path);

  Glib::OptionContext context;
  context.set_main_group(options);
  

  // Make Sure we've got a username and password.
  bool parse_failed = false;
  try
  {
    context.parse(argc, argv);
  }
  catch(const Glib::Error&)
  {
    print_error_msg("ERROR: Could not parse arguments.");
    parse_failed = true;
  }

  if(username.empty())
  {
    print_error_msg("ERROR: no PostgreSQL username was provided.");
    parse_failed = true;
  }

  if(password.empty())
  {
    print_error_msg("ERROR: no PostgreSQL password was provided.");
    parse_failed = true;
  }

  if(config_path.empty())
  {
    print_error_msg("ERROR: no PostgreSQL configuration file path was provided.");
    parse_failed = true;
  }

  if(parse_failed)
  {
    std::cerr << std::endl << context.get_help() << std::endl;
    return ErrorCodes::MISC_BAD_ARGUMENTS;
  }

  // Change config file.
  int config_exit_code = set_listen_on_all_interfaces(config_path);
  if(!(config_exit_code == SuccessCodes::CHANGES_APPLIED ||
       config_exit_code == SuccessCodes::CHANGES_NOT_NEEDED))
  {
    return config_exit_code;
  }

  // Create new user.
  const int create_user_exit_code = create_postgre_user(username, password);

  return create_user_exit_code;
}
