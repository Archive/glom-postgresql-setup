/*
 *  Glom PostgreSQL Setup - configuration.cc
 *
 *  Copyright (C) 2011 Openismus GmbH
 *
 *  Author: Jon Nordby <jonn@openismus.com>
 *  Author: Chris Kühl <chrisk@openismus.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib> /* for getenv() */
#include <iostream>

#include <glibmm.h>

#include "configuration.h"
#include "error-codes.h"

#include "../config.h"

Configuration::Configuration()
: m_config_path( Glib::build_filename(POSTGRES_CONFIG_PATH, "postgresql.conf") )
{
}

void Configuration::set_username(const Glib::ustring& username)
{
  m_username = username;
}

void Configuration::set_password(const Glib::ustring& password)
{
  m_password = password;
}

void Configuration::set_config_path(const std::string& config_path)
{
  m_config_path = config_path;
}

Glib::ustring Configuration::get_username() const
{
  return m_username;
}

Glib::ustring Configuration::get_password() const
{
  return m_password;
}

std::string Configuration::get_config_path() const
{
  return m_config_path;
}

Glib::ustring Configuration::get_default_username() const
{
  char* username = getenv("USER");
  if(username)
    return Glib::ustring(username);
  else
    return Glib::ustring();
}

// Returns the exit code of the target executable.
int Configuration::apply() const
{
  // Return error code if we don't have a username.
  if(get_username().empty())
    return ErrorCodes::USER_NAME_EMPTY;

  // Return error code if we don't have a password.
  if(get_password().empty())
    return ErrorCodes::USER_PASSWORD_EMPTY;

  // Return error code if we don't have a config_path.
  if(get_config_path().empty())
    return ErrorCodes::CONFIG_PATH_EMPTY;

  // Build the command line
  // TODO: This should not be hardcoded.
  const Glib::ustring command = 
    Glib::ustring::compose("pkexec " SBINDIR "/glom-postgresql-setup-helper --username %1 --password %2 --config-path %3",
      get_username(),
      get_password(),
      get_config_path());

  int exit_status = 0;
  Glib::spawn_command_line_sync(command, 0, 0, &exit_status);

  if(WIFEXITED(exit_status))
    return WEXITSTATUS(exit_status);
  else
    return ErrorCodes::MISC_PROGRAM_EXITED_ABNORMALLY;
}

