/*
 *  Glom PostgreSQL Setup - config-dialog.cc
 *
 *  Copyright (C) 2011 Openismus GmbH
 *
 *  Author: Jon Nordby <jonn@openismus.com>
 *  Author: Chis Kühl <chrisk@openismus.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>

#include <gtkmm/messagedialog.h>

#include "config-dialog.h"
#include "error-codes.h"

#include <glibmm/i18n.h>

namespace {

const int MAX_ENTRY_LENGTH = 128;

// Utility function to build error string.
static Glib::ustring format_error(const Glib::ustring& text)
{
  return Glib::ustring::compose(_("<i><b>Error</b>: %1.</i>"), text);
}

} // End namespace

ConfigDialog::ConfigDialog(const Glib::ustring& title)
: Gtk::Dialog(title),
  m_description_label(_("Create a new PostgreSQL database server user and modify the PostgreSQL configuration file for Glom."), 0.0f),
  m_error_label(Glib::ustring(), 0.0),
  m_input_table(4, 3),
  m_username_label(_("Username:"), 0.0),
  m_password_label(_("Password:"), 0.0),
  m_password_confirm_label(_("Confirm Password:"), 0.0),
  m_config_path_label(_("Configuration File:"), 0.0),
  m_config_path_button(_("Browse…"))
{
  initialize_widgets();
}

/* Initialize the widgets.
 * Sets default widget attributes, builds the dialog, hooks up signals
 * and calls show_all() on the internal vbox of the dialog. */
/* TODO: Make it clear in the UI what the dialog will do */
/* TODO: line up elements properly, its ugly now */
void ConfigDialog::initialize_widgets()
{
  set_border_width(12);
  set_size_request(500, 100);
  set_resizable(false);

  Gtk::Box* config_widget_box = get_content_area();
  config_widget_box->set_spacing(6);
  config_widget_box->set_border_width(6);

  m_input_table.set_border_width(6);
  m_input_table.set_spacings(6);

  /* Header */
  m_description_label.set_line_wrap(true);

  /* Username */
  m_username_label.set_alignment(0.0);
  m_username_entry.set_max_length(MAX_ENTRY_LENGTH);
  m_username_entry.set_text(config.get_default_username());
  m_input_table.attach(m_username_label, 0, 1, 0, 1, Gtk::SHRINK | Gtk::FILL);
  m_input_table.attach(m_username_entry, 1, 3, 0, 1, Gtk::EXPAND | Gtk::FILL);

  /* Password */
  m_password_label.set_alignment(0.0);
  m_password_entry.set_visibility(false);
  m_password_entry.set_activates_default();
  m_password_entry.set_max_length(MAX_ENTRY_LENGTH);
  m_input_table.attach(m_password_label, 0, 1, 1, 2, Gtk::SHRINK | Gtk::FILL);
  m_input_table.attach(m_password_entry, 1, 3, 1, 2, Gtk::EXPAND | Gtk::FILL);

  /* Confirm Password */
  m_password_confirm_label.set_alignment(0.0);
  m_password_confirm_entry.set_visibility(false);
  m_password_confirm_entry.set_activates_default();
  m_password_confirm_entry.set_max_length(MAX_ENTRY_LENGTH);
  m_input_table.attach(m_password_confirm_label, 0, 1, 2, 3, Gtk::SHRINK | Gtk::FILL);
  m_input_table.attach(m_password_confirm_entry, 1, 3, 2, 3, Gtk::EXPAND | Gtk::FILL);

  m_config_path_label.set_alignment(0.0);
  m_config_path_entry.set_text(config.get_config_path());
  m_input_table.attach(m_config_path_label, 0, 1, 3, 4, Gtk::SHRINK | Gtk::FILL);
  m_input_table.attach(m_config_path_entry, 1, 2, 3, 4, Gtk::EXPAND | Gtk::FILL);
  m_input_table.attach(m_config_path_button, 2, 3, 3, 4, Gtk::SHRINK | Gtk::FILL);

  m_config_path_button.signal_clicked().connect( sigc::mem_fun(*this,
    &ConfigDialog::set_path_from_file_chooser) );

  /* Error feedback */
  m_error_label.set_visible(false);
  m_error_label.set_use_markup(true);

  /* Pack widgets */
  //TODO: Align the label to the left:
  config_widget_box->pack_start(m_description_label);
  config_widget_box->pack_start(m_input_table);

  /* Add dialog buttons */
  add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  add_button(_("Create"), Gtk::RESPONSE_ACCEPT);

  set_default_response(Gtk::RESPONSE_ACCEPT);

  show_all_children();
}

void ConfigDialog::set_path_from_file_chooser()
{
  Gtk::FileChooserDialog dialog(_("Please choose a PostgreSQL configuration file."));
  dialog.set_transient_for(*this);

  dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);

  Glib::RefPtr<Gtk::FileFilter> filter_conf = Gtk::FileFilter::create();
  filter_conf->set_name(_("Config file"));
  filter_conf->add_mime_type("text/plain");
  filter_conf->add_pattern("*.conf");
  dialog.add_filter(filter_conf);

  //Start with the existing path:
  //TODO: Only if this can actually be read as the current user.
  //On Fedora, you can't even see that the file exists.
  dialog.set_filename( m_config_path_entry.get_text() );

  const int result = dialog.run();

  if(result == Gtk::RESPONSE_OK)
    m_config_path_entry.set_text(dialog.get_filename());
}

void ConfigDialog::response_accept()
{
  int exit_code = -1;

  if(m_password_entry.get_text().empty() ||
    (m_password_entry.get_text() == m_password_confirm_entry.get_text()))
  {
    /* Update configuration */
    config.set_username(m_username_entry.get_text());
    config.set_password(m_password_entry.get_text());
    config.set_config_path(m_config_path_entry.get_text());

    /* Execute the changes */
    exit_code = config.apply();
  }
  else
  {
    // Clear passwords.
    m_password_entry.set_text(Glib::ustring());
    m_password_confirm_entry.set_text(Glib::ustring());
    exit_code = ErrorCodes::USER_PASSWORDS_DONT_MATCH;
  }

  // Respond to return code
  Glib::ustring error_text;
  switch (exit_code)
  {
    case SuccessCodes::CHANGES_APPLIED:
      {
        Gtk::MessageDialog dialog(*this, _("Changes applied to PostgreSQL configuration file."));
        dialog.set_secondary_text(_("You'll need to restart the PostgreSQL server for changes to take effect."));
        dialog.set_transient_for(*this);
        dialog.run();
        hide();
      }
      break;
    case SuccessCodes::CHANGES_NOT_NEEDED:
      {
        Gtk::MessageDialog dialog(*this, _("The configuration was successful."));
        dialog.set_secondary_text(_("You may now use the PostgreSQL server from Glom as a central server."));
        dialog.set_transient_for(*this);
        dialog.run();
        hide();
      }
      break;
    case ErrorCodes::MISC_NOT_ROOT:
      error_text = _("Root access not obtained");
      break;
    case ErrorCodes::MISC_BAD_ARGUMENTS:
      error_text = _("Error parsing arguments");
      break;
    case ErrorCodes::MISC_PROGRAM_EXITED_ABNORMALLY:
      error_text = _("Helper program exited abnormally");
      break;
    case ErrorCodes::MISC_AUTHENTICATION_FAILED:
      error_text = _("Could not acquire administrative privileges");
      break;
    case ErrorCodes::DB_CONNECTION_NULL:
      error_text = _("Problem connecting to database");
      break;
    case ErrorCodes::DB_CONNECTION_BAD:
      error_text = _("Connection error");
      break;
    case ErrorCodes::USER_ALREADY_EXISTS:
      error_text = _("User already exists. Try a new user name");
      break;
    case ErrorCodes::USER_DISALLOWED_CHAR:
      error_text = _("Only alpha-numeric characters allowed");
      break;
    case ErrorCodes::USER_NAME_EMPTY:
      error_text = _("No username supplied");
      break;
    case ErrorCodes::USER_PASSWORDS_DONT_MATCH:
      error_text = _("Passwords do not match");
      break;
    case ErrorCodes::USER_PASSWORD_EMPTY:
      error_text = _("No password supplied");
      break;
    case ErrorCodes::CONFIG_PATH_EMPTY:
      error_text = _("No PostgreSQL configuration file path supplied");
      break;
    case ErrorCodes::CONFIG_FILE_NOT_FOUND:
      error_text = _("Postgres configuration file not found");
      break;
    case ErrorCodes::CONFIG_FILE_ERROR:
      error_text = _("Error creating or accessing file");
      break;
    case ErrorCodes::CONFIG_IO_STATUS_ERROR:
      error_text = _("Error reading or writing file");
      break;
    case ErrorCodes::CONFIG_RENAME_FAILED:
      error_text = _("Could not move file");
      break;
    default:
      error_text = _("Unknown error");
      break;
  }

  // If it's not visible, make it so and attach.
  if(!m_error_label.get_visible())
  {
    m_error_label.set_visible(true);

    Gtk::Box* config_widget_box = get_content_area();
    config_widget_box->pack_start(m_error_label);
  }

  m_error_label.set_markup(format_error(error_text));
}

void ConfigDialog::on_response(int response_id)
{
  if(response_id == Gtk::RESPONSE_ACCEPT)
    response_accept();

  if(response_id == Gtk::RESPONSE_CANCEL)
    hide();

  /* If the window was passed to Gtk::Main::run(),
   * hiding it will quit the application */
}
